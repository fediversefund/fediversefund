import React from "react";

type Props = {};

const Header = (props: Props) => {
  return (
    <header className="text-white p-4 flex justify-center items-center">
      <h1 className="text-2xl font-bold">Fediverse Funding</h1>
    </header>
  );
};

export default Header;
