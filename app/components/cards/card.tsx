import { Fund } from "@/app/funds";
import React from "react";
import Link from "next/link";
import Image from "next/image";

const card = ({
  imgSrc = "/fediverse.png",
  title,
  description,
  link,
  fundingMethods,
}: Fund) => {
  return (
    <div className="max-w-sm rounded overflow-hidden shadow-lg bg-slate-50 flex-auto p-4 text-black font-medium h-auto m-2">
      <div className="flex flex-col text-center items-center">
        <Image
          src={imgSrc}
          alt={"Logo for" + title}
          width="200"
          height="200"
          className="p-4"
        />
        <Link
          className="font-semibold text-lg text-sky-600 underline"
          href={link}
        >
          {title}
        </Link>
        <p className="text-md pb-6">{description}</p>
        <p className="font-semibold">How to contribute?</p>
        {fundingMethods.map((fundingMethod) => (
          <Link
            key={fundingMethod.link}
            className="text-md text-sky-600 underline"
            href={fundingMethod.link}
          >
            {fundingMethod.name}
          </Link>
        ))}
      </div>
    </div>
  );
};

export default card;
