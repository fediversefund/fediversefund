"use client";
import React, { useState } from "react";
import Card from "./card";
import { FundList } from "@/app/funds";
import Search from "./search";

type Props = {};

function CardView({}: Props) {
  const [searchText, setSearchText] = useState("");

  const filteredFundList = FundList.filter(({ title }) =>
    title.toUpperCase().includes(searchText.toUpperCase())
  );
  return (
    <>
      <div className="flex justify-center">
        <Search setSearchText={setSearchText} />
      </div>
      <div className="flex justify-center">
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5">
          {filteredFundList.map((fundItem) => {
            return <Card key={fundItem.link} {...fundItem} />;
          })}
        </div>
      </div>
    </>
  );
}

export default CardView;
