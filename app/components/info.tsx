import React from "react";

type Props = {};

const info = (props: Props) => {
  return (
    <div className="text-center p-4 pb-10">
      <p>Helping the fediverse keep the lights on</p>
      <a
        className="text-red-400"
        href="https://gitlab.com/fediversefund/fediversefund#getting-your-project-added"
      >
        Do you need funding? Go here.
      </a>
    </div>
  );
};

export default info;
