import { Fragment } from "react";
import Header from "./components/header";
import Info from "./components/info";
import CardView from "./components/cards/card-view";

export default function Home() {
  return (
    <Fragment>
      <Header />
      <Info />
      <CardView />
    </Fragment>
  );
}
