// Temporarly keeping this here. Eventually want to store in some db
interface FundingMethods {
  name: string;
  link: string;
}

export interface Fund {
  imgSrc?: string;
  title: string;
  description: string;
  link: string;
  fundingMethods: FundingMethods[];
}

export const FundList: Fund[] = [
  {
    imgSrc: "/fund-images/kbin.webp",
    title: "Kbin",
    description:
      "Kbin is a decentralized content aggregator and microblogging platform running on the Fediverse network",
    link: "https://kbin.social",
    fundingMethods: [
      {
        name: "Buy Me A Coffee",
        link: "https://www.buymeacoffee.com/kbin",
      },
    ],
  },
  {
    imgSrc: "/fund-images/lemmy.svg",
    title: "Lemmy",
    description:
      "Lemmy is a selfhosted social link aggregation and discussion platform. It is completely free and open, and not controlled by any company.",
    link: "https://join-lemmy.org/",
    fundingMethods: [
      {
        name: "Support Lemmy",
        link: "https://join-lemmy.org/donate",
      },
      {
        name: "Liberapay",
        link: "https://liberapay.com/Lemmy",
      },
      {
        name: "Patreon",
        link: "https://www.patreon.com/dessalines",
      },
      {
        name: "Open Collective",
        link: "https://opencollective.com/lemmy",
      },
    ],
  },
  {
    imgSrc: "/fund-images/beehaw.png",
    title: "Beehaw",
    description:
      "We’re a collective of individuals upset with the way social media has been traditionally governed.",
    link: "https://beehaw.org/",
    fundingMethods: [
      {
        name: "Open Collective",
        link: "https://opencollective.com/beehaw",
      },
    ],
  },
  {
    imgSrc: "/fund-images/lemmy-world.webp",
    title: "Lemmy.world",
    description: "A Lemmy site for various topics, for everyone to use.",
    link: "https://lemmy.world/",
    fundingMethods: [
      {
        name: "Open Collective",
        link: "https://opencollective.com/mastodonworld",
      },
      {
        name: "Patreon",
        link: "https://www.patreon.com/mastodonworld",
      },
    ],
  },
  {
    imgSrc: "/fund-images/mastodon.png",
    title: "Mastodon",
    description: "Social networking that's not for sale.",
    link: "https://joinmastodon.org/",
    fundingMethods: [
      {
        name: "Become a sponsor",
        link: "https://sponsor.joinmastodon.org/",
      },
      {
        name: "Patreon",
        link: "https://www.patreon.com/mastodon",
      },
    ],
  },
];
