A website to help keep track of ways to support fediverse projects and instances

## Getting Your Project Added

Right now the system is pretty barebones. Will be adding a more formal process in the coming weeks if there is interest in this project.

If you know your way around git, feel free to open up a PR and add your fund to `app/funds.tsx`

Otherwise, open up an issue in the issue tracker with the title format "[Fund] Project Name" and the following info:

```
Name: Name of your project
Link: Link to your project
Description: SHORT (1-2 sentences) description of your project
Image: Link to an image that can be used

Funding Options:
Name of funding option - Link to funding option
Name of funding option - Link to funding option
...etc
```

## Upcoming Features

At this state, the app is pretty bare bones. Just wanted to get something up fast. If there is a feature you would like to see, feel free to pop open an issue on the issue tracker with the title "[Feature] Whatever your idea is"

In terms of immediate features that I want to see added

- Formal process to add new projects
- Backend to serve project list (right now just a static list on the front end)
- Better searching (right now just a simple text search)
- Virtualizing list (right now not an issue, but can be an issue as more funds get added)
- Improved UI

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
